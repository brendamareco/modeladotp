from pyscipopt import Model, quicksum
class Demanda: 
    def __init__(self, cantidad, ancho):
        self.cantidad = cantidad
        self.ancho = ancho

    def __eq__(self, other):
        return self.cantidad == other.cantidad and self.ancho == other.ancho

    # Comparacion
    def __lt__(self, other):
        return self.ancho > other.ancho

# Funcion para leer los datos del archivo
def getDatos(path):
    archivo = open(path, "r")
    listaLineas = []
    
    for linea in archivo:
        if (linea[0]!= "#"):
            listaLineas.append(linea)
    
    archivo.close()
    return listaLineas

def ordenarMayorMenor(rollosDemandados):
    ordenamiento = sorted(rollosDemandados)
    return  [rolloDemandado for rolloDemandado in ordenamiento]

def generarPatrones(patron,patrones, demandas, anchoRollo):
    if (len(patrones) < 1): #Si es el primer patron que voy a generar
        setNuevoPatron(patron, demandas, anchoRollo,0)
        patrones.append(patron)

    else:  #Generar patrón a partir del último que generé 
        esUltimoPatron = True
        for i in range(len(demandas)-2,0,-1): # Recorro al reves desde el anteultimo

            if (patron[i] == 0):
                if (patron[i-1] != 0):
                    patron[i-1] = patron[i-1]-1
                    setNuevoPatron(patron, demandas, anchoRollo ,i)
                    patrones.append(patron)
                    esUltimoPatron = False
                    break 
            else:
                esUltimoPatron = False
                patron[i] = patron[i]-1
                setNuevoPatron(patron, demandas, anchoRollo, i+1)
                patrones.append(patron)
                break

        if (esUltimoPatron):
            return

    patron = patrones[len(patrones)-1][:] #Para crear un patron a partir del ultimo agregado 
    generarPatrones(patron,patrones, demandas, anchoRollo)

def setNuevoPatron(patron,demandas, anchoRollo, indice):
    resto = anchoRollo
    for i in range(0,len(demandas)):
        if (i >= indice):
            patron[i] = getMaximal(demandas[i].ancho,resto)
        resto = resto - patron[i]*demandas[i].ancho

def getMaximal(ancho,resto):
    maximo = 0
    encontreMaximal = False

    while (encontreMaximal == False):
        if (0 >= resto):
            return 0
        elif (maximo*ancho > resto):
            return maximo-1

        maximo = maximo + 1

def coeficientesUbicacionPatrones(patrones, indiceCorte):
    res = []
    for i in range (len(patrones)):
        if (patrones[i][indiceCorte] > 0):
            res.append([patrones[i][indiceCorte], i])
    return res


def crearPL(patrones, demandas):
    programaLineal = Model("TP - Programa Lineal con variables enteras")
    listaVariables = []

    # Creacion de las variables del Programa Lineal
    for i in range (1, len(patrones)+1):
        nombreVariable = "x"+str(i)
        variable = programaLineal.addVar(vtype="I", name=nombreVariable)
        listaVariables.append(variable)

    # Restricciones
    for i in range (len (patrones[0])):
        listaCoeficientesPosiciones = coeficientesUbicacionPatrones(patrones, i)
        programaLineal.addCons(quicksum( coeficientesPosiciones[0]*listaVariables[coeficientesPosiciones[1]] for coeficientesPosiciones in listaCoeficientesPosiciones ) >= demandas[i].cantidad)
   
    # Funcion objetivo
    programaLineal.setObjective(quicksum(variable for variable in listaVariables))

    programaLineal.optimize()

    #Si encontramos el optimo imprimimos los valores de las variables.
    if (programaLineal.getStatus() == "optimal"):
        print("Optimal value:", programaLineal.getObjVal())
        print("Solution:")
        for i in range (len(listaVariables)):
            print (" x" + str(i+1) + " = " + str (programaLineal.getVal(listaVariables[i])) )
        print("En base a los siguientes patrones: \n" + str(patrones))
    else:
        print("Problem could not be solved to optimality")

def main():
    patrones = []
    patron = []
    datos = getDatos("rollos.in")
    anchoRollo = int(datos[0])
    demandasArchivo = []

    for i in range(1, len(datos)):
        linea = datos[i].split(":")
        cantidad = int (linea[0])
        ancho = int (linea[1])
        demandasArchivo.append(Demanda(cantidad, ancho))

    demandas = []    
    demandas = ordenarMayorMenor(demandasArchivo)

    for i in range(len(demandas)):
        patron.append(0)

    generarPatrones(patron,patrones, demandas, anchoRollo)    

    # Programa Lineal
    crearPL(patrones, demandas)

if __name__ == "__main__":
    main()
